#!/usr/bin/env python3



import time
import json
import locale
import subprocess
import os
import requests
import gi

gi.require_version("Unity", "7.0")
gi.require_version("Gtk", "3.0")
gi.require_version("Notify", "0.7")
gi.require_version("AppIndicator3", "0.1")
from gi.repository import Unity, GObject, Gtk as gtk, Notify, Gdk, Pango, GLib
from gi.repository import AppIndicator3 as appindicator
import logging
from bitconfig import ConfigManager
import threading


class TickerIndicator(object):
    '''
    classdocs
    '''

    _HOME_DIR = os.environ["SNAP_USER_COMMON"] + '/BitTicker'
    log_file = _HOME_DIR + "/bitticker.log"
    item = 0
    price = 0
    s = 0
    thread = 0
    ind = None
    updateLength = 10
    prevPrice = 0
    attnLength = 5
    icon_dir = None

    cfg = None

    try:
        icon_dir = os.environ["SNAP"] + "/meta/gui/"
    except:
        icon_dir = "/home/smithi/repository/bitticker/snap/snap/gui/"

    notify_icon_up = icon_dir + "btcnotify-up.xpm"
    notify_icon_down = icon_dir + "btcnotify.xpm"

    def __init__(self):  # , params ):
        '''
        Constructor
        '''
        # clean our log file for now
        if (os.path.isfile(self.log_file)):
            os.remove(self.log_file)

        # setup ConfigManager
        self.cfg = ConfigManager(self._HOME_DIR)

        logging.basicConfig(filename=self.log_file, level=logging.DEBUG,
                            format='(%(asctime)s %(levelname)-9s) %(message)s', )
        self.ind = appindicator.Indicator.new("bitcoin-ticker", self.icon_dir + "btc.xpm",
                                              appindicator.IndicatorCategory.APPLICATION_STATUS)
        self.ind.set_status(appindicator.IndicatorStatus.ACTIVE)
        self.ind.set_attention_icon(self.notify_icon_up)
        self.ind.set_label("Loading", "Loading")

        # create a menu
        self.menu = gtk.Menu()

        # create items for the menu - labels, checkboxes, radio buttons and images are supported:

        image = gtk.MenuItem("Quit")
        image.connect("activate", self.quit)
        image.show()
        self.menu.append(image)

        self.menu.show()

        self.ind.set_menu(self.menu)

        self.s = threading.Timer(int(self.cfg.getProperty("check-interval")), self.checkForUpdates, ())
        self.checkForUpdates()

    def setS(self, s):
        self.s = s

    def quit(self, widget, data=None):
        if (self.s != 0):
            self.s.cancel()
        gtk.main_quit()

    def checkForUpdates(self, widget=None, data=None):
        '''
        Check for updates from Bittrex.com
        '''
        th = 0

        url = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=%s" % (self.cfg.getProperty("currency"))
        resp = requests.get(url=url)
        data = json.loads(resp.text)
        price = data["result"][0]["Last"]
        if (self.s.isAlive()):
            th = self.s
            if (self.prevPrice > 0):
                if (self.prevPrice != price):
                    logging.info("PRICE UPDATE: Old Price - %i, New Price - %i", self.prevPrice, price)

                    if (self.prevPrice < price):
                        self.ind.set_attention_icon(self.notify_icon_down)
                    else:
                        self.ind.set_attention_icon(self.notify_icon_up)
                    self.attnOn()

                    threading.Timer(self.attnLength, self.attnOff, ()).start()
            self.prevPrice = price

        self.ind.set_label(str(price), str(price))
        logging.info("Creating new timer")
        self.s = threading.Timer(self.updateLength, self.checkForUpdates, ())
        self.s.start()

        if (self.s == th):
            th.cancel()

        logging.info('Timer Created! Scheduled for %i seconds', self.updateLength)

    def attnOn(self):
        logging.debug("ICON STATUS: ATTENTION")
        self.ind.set_status(appindicator.IndicatorStatus.ATTENTION)

    def attnOff(self):
        logging.debug("ICON STATUS: NORMAL")
        self.ind.set_status(appindicator.IndicatorStatus.ACTIVE)


def main():
    gtk.main()

    return 0


if __name__ == "__main__":
    indicator = TickerIndicator()
    main()
