import configparser
import os


class ConfigManager():
    _CONFIG_FILE = None
    _SETTINGS = None

    def __init__(self, CONFIG_FILE_FOLDER):
        CONFIG_FILE = CONFIG_FILE_FOLDER + "/config.cfg"

        if (os.path.exists(CONFIG_FILE)):
            _exists = True
        else:
            os.mkdir(CONFIG_FILE_FOLDER)
            _exists = False
        self._CONFIG_FILE = CONFIG_FILE

        self._SETTINGS = configparser.ConfigParser()
        self._SETTINGS._interpolation = configparser.ExtendedInterpolation()

        if not (_exists):
            self._createConfig(self._CONFIG_FILE)

        self._SETTINGS.read(self._CONFIG_FILE)

    def _createConfig(self, CONFIG_FILE):
        cfgfile = open(CONFIG_FILE, 'w')
        self._SETTINGS.add_section('Main')

        self._SETTINGS.set('Main', 'currency', 'USDT-BTC')
        self._SETTINGS.set('Main', 'check-interval', "10")

        self._SETTINGS.write(cfgfile)
        cfgfile.close()

    def _configSectionMap(self, section):
        # https://wiki.python.org/moin/ConfigParserExamples
        dict1 = {}
        options = self._SETTINGS.options(section)
        for option in options:
            try:
                dict1[option] = self._SETTINGS.get(section, option)
            except:
                dict1[option] = None
        return dict1

    def getProperty(self, name=None):
        return self._configSectionMap('Main')[name]
