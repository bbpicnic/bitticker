#!/usr/bin/env python3

from distutils.core import setup
from DistUtilsExtra.command import build_i18n, build_extra

# DEST="/opt/extras.ubuntu.com/teatime/"
DEST = "usr/share/bitticker/"

setup(
    cmdclass={"build": build_extra.build_extra},
    name="bitticker",
    description="A live bitcoin ticker for your system tray",
    author="Ian Smith",
    author_email="ian@smithi.co.uk",
    url="http://www.smithi.co.uk",
    license="GNU GPL v3",
    data_files=[(DEST, ["bitticker.py"]),
                (DEST + "bitconfig/", ['bitconfig/__init__.py'])])
