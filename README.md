# BitTicker

A live bitcoin ticker for your system tray

## Getting Started

Simply open BitTicker from your apps or type 'bitticker' into the console

### Prerequisites

you need to install snapd to run snaps

### Installing

- sudo snap install bitticker

## Authors

* **Ian Smith** - [Launchpad](https://launchpad.net/~smith.i)

## License

This project is licensed under the GPL v3 License

